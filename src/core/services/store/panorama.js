import Proj4 from 'proj4'
import mapGeojson from '../../../assets/data/coba.geo.json'

let set_utm_proj =  function(utm_code){
  let utm_zone = utm_code.slice(0,-1)
  const utm_suffix = utm_code.slice(-1)
  if (!"NPQRSTUVZXY".includes(utm_suffix)) { utm_zone += '+south' }
  return "+proj=utm +zone=%z +datum=WGS84 +units=m +no_defs".replace("%z", utm_zone) // + ("N" == utm_or ? "" : "+south"));
}


export default {
  state: {
    panorama_load:[],
    currentPano: '',
    Pano_current: {},
    cursor_pano: [0, 0],
    geojson: mapGeojson,
    cursor_x : 0,
    cursor_y : 0
  },
  getters: {
    get_panoramaLoad(state){
      // let current = state.panorama_load.filter(item => item.id != state.currentPano)
      // console.log(state.currentPano[0].id)
      // return current
      return state.panorama_load
    },
    get_currentPano(state){
      return state.currentPano
    },
    get_pano_current(state){
      return state.Pano_current
    },
    get_pano(state) {
        return [state.Pano_current.lon, state.Pano_current.lat];
    },
    get_cursor_pano(state){
      return state.cursor_pano
    },
    get_map_geojson(state){
      return state.geojson
    },
    get_utm_proj(state){
      return set_utm_proj(state.Pano_current.utm_code)
    },
    get_cursor_x(state){
      return state.cursor_x
    },
    get_cursor_y(state){
      return state.cursor_y
    },
  },
  actions: {
    cursor_pano(context, data){
      let cursor_x = context.state.Pano_current.utm_x + data.x;
      let cursor_y = context.state.Pano_current.utm_y + data.y;

      let point = Proj4( set_utm_proj(context.state.Pano_current.utm_code), "EPSG:4326", [cursor_x, cursor_y]);
      context.commit('change_cursor_pano', point)
      context.commit('change_cursor_x', cursor_x)
      context.commit('change_cursor_y', cursor_y)
    }
  },
  mutations: {
    change_panoramaLoad(state, data){
        state.panorama_load = data
    },
    change_currentPano(state, data){
      state.currentPano = data
    },
    change_pano_current(state, data){
      state.Pano_current = data
    },
    change_cursor_pano(state, data){
      state.cursor_pano = data
    },
    change_cursor_x(state, data){
      state.cursor_x = data
    },
    change_cursor_y(state, data){
      state.cursor_y = data
    },
  }
};

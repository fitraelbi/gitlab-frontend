

export default {
  state: {
    map_select: "osm",
    zoom: 18,
    center: [0, 0],
    enablesideMap: false,
    mapActive:true,
    tagActive: false,
    spotsActive: false,
    rotate_pano: 0,
    mapspot: []
  },
  getters: {
    get_basemap(state) {
      return state.map_select;
    },
    get_zoom(state) {
      return state.zoom;
    },
    get_center(state) {
      return state.center;
    },
    get_enablesideMap(state) {
      return state.enablesideMap;
    },
    get_mapActive(state){
      return state.mapActive
    },
    get_tagActive(state){
      return state.tagActive
    },
    get_rotate_map_pano(state){
      return state.rotate_pano
    },
    get_spotsActive(state){
      return state.spotsActive
    },
    get_mapspot(state){
      return state.mapspot
    }
  },
  actions: {
    rotate_map_pano(context, data){
      context.commit('change_rotatepano', data * Math.PI / 180)
    },
  },
  mutations: {
    change_basemap(state, data) {
      // console.log('mutation')
      // console.log(data)
      // console.log('you')
      state.map_select = data;
    },
    change_zoom(state, data) {
      state.zoom = data;
    },
    change_center(state, data) {
      state.center = data;
    },
    change_enablesideMap(state, data) {
      state.enablesideMap = data;
    },
    change_mapActive(state){
      state.mapActive = !state.mapActive
    },
    change_tagActive(state){
      state.tagActive = !state.tagActive
    },
    change_spotsActive(state){
      state.spotsActive = !state.spotsActive
    },
    change_rotatepano(state, data){
      state.rotate_pano = data
    },
    change_mapspot(state, data){
      state.mapspot = data
    }
  }
};

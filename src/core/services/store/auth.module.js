// import ApiService from "@/core/services/api.service";
import JwtService from "@/core/services/jwt.service";
import axios from "axios";

// action types
export const VERIFY_AUTH = "verifyAuth";
export const LOGIN = "login";
export const LOGOUT = "logout";
export const REGISTER = "register";
export const UPDATE_USER = "updateUser";

// mutation types
export const PURGE_AUTH = "logOut";
export const SET_AUTH = "setUser";
export const SET_ERROR = "setError";

export const SET_DATA_USER = "setdatauser";
export const MODIFY_USER = "modifyuser";
export const TOKEN = "token";

const jwt = require("jsonwebtoken");

const state = {
  errors: null,
  user: {},
  isAuthenticated: !!JwtService.getToken(),
  user_data: {
    id: "",
    email: "",
    username: "",
    firstname: "",
    lastname: "",
    role: 0
  },
  token: ""
};

const getters = {
  currentUser(state) {
    return state.user;
  },
  isAuthenticated(state) {
    return state.isAuthenticated;
  },
  getUserData(state) {
    let params = {
      id: state.user_data.id,
      email: state.user_data.email,
      username: state.user_data.username,
      firstname: state.user_data.firstname,
      lastname: state.user_data.lastname,
      role: state.user_data.role
    };
    return params;
  },
  getToken(state) {
    return state.token;
  }
};

const actions = {
  // [LOGIN](context, credentials) {
  //   return new Promise(resolve => {
  //     ApiService.post("login", credentials)
  //       .then(({ data }) => {
  //         context.commit(SET_AUTH, data);
  //         resolve(data);
  //       })
  //       .catch(({ response }) => {
  //         context.commit(SET_ERROR, response.data.errors);
  //       });
  //   });
  // },
  [LOGOUT](context) {
    context.commit(PURGE_AUTH);
  },
  // [REGISTER](context, credentials) {
  //   return new Promise((resolve, reject) => {
  //     ApiService.post("users", { user: credentials })
  //       .then(({ data }) => {
  //         context.commit(SET_AUTH, data);
  //         resolve(data);
  //       })
  //       .catch(({ response }) => {
  //         context.commit(SET_ERROR, response.data.errors);
  //         reject(response);
  //       });
  //   });
  // },
  async [VERIFY_AUTH](context) {
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    const token = urlParams.get("token");
    if (JwtService.getToken()) {
      let token = JwtService.getToken();
      let url = process.env.VUE_APP_URL.concat(`/verify/`);
      await axios
        .post(url, { token })
        .then(() => {
          let decode = jwt.decode(token);
          context.commit(TOKEN, token);
          context.commit(SET_DATA_USER, decode);
        })
        .catch(() => {
          context.commit(PURGE_AUTH);
        });
    } else if(token!=null){
      JwtService.saveToken(token);
      let url = process.env.VUE_APP_URL.concat(`/verify/`);
      await axios
        .post(url, { token })
        .then(() => {
          let decode = jwt.decode(token);
          context.commit(TOKEN, token);
          context.commit(SET_DATA_USER, decode);
        })
        .catch(() => {
          context.commit(PURGE_AUTH);
        });
    }else {
      context.commit(PURGE_AUTH);
    }
  },
  // [UPDATE_USER](context, payload) {
  //   const { email, username, password, image, bio } = payload;
  //   const user = { email, username, bio, image };
  //   if (password) {
  //     user.password = password;
  //   }

  //   return ApiService.put("user", user).then(({ data }) => {
  //     context.commit(SET_AUTH, data);
  //     return data;
  //   });
  // }
};

const mutations = {
  [SET_ERROR](state, error) {
    state.errors = error;
  },
  [SET_AUTH](state) {
    state.isAuthenticated = true;
    JwtService.saveToken(state.user.token);
  },
  [PURGE_AUTH](state) {
    state.isAuthenticated = false;
    state.user = {};
    state.errors = {};
    JwtService.destroyToken();
  },
  [SET_DATA_USER](state, data) {
    state.user_data.id = data.user_id;
    state.user_data.email = data.email;
    state.user_data.username = data.username;
    state.user_data.firstname = data.first_name;
    state.user_data.lastname = data.last_name;
    state.user_data.role = data.role;
  },
  [TOKEN](state, data) {
    state.token = data;
  }
};

export default {
  state,
  actions,
  mutations,
  getters
};

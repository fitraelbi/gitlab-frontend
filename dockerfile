FROM node:lts-alpine as build-stage
WORKDIR /app
COPY package.json /app/package.json
RUN yarn install 
RUN yarn add @vue/cli -g
COPY . /app
RUN yarn run build

FROM nginx:stable-alpine as production-stage
COPY --from=build-stage /app/dist /usr/share/nginx/html
RUN rm /etc/nginx/conf.d/default.conf
COPY ./nginx.conf /etc/nginx/conf.d
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
